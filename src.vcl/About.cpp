/*
 * About.cpp - the About box
 * Copyright (C) 2012-2013  Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vcl.h>
#pragma hdrstop

#include "About.h"
#pragma package(smart_init)

#include "Strings.hpp"

/*
 * Expands a macro as a wide string literal.
 */
#define W_(s) W__(s)
#define W__(s) L ## s

#pragma resource "*.dfm"
TAboutBox *AboutBox;

__fastcall TAboutBox::TAboutBox(TComponent *AOwner) : TForm(AOwner) {
    UnicodeString productName = Strings_ProductName;

    TVarRec formatArgs[] = {productName, W_(PACKAGE_VERSION)};
    Caption = Format(Caption, formatArgs, 1);
    Label1->Caption = Format(Label1->Caption, formatArgs, 1);
}
