/*
 * About.h - the About box
 * Copyright (C) 2012-2013  Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AboutH
#define AboutH 1

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

class TAboutBox : public TForm {
__published:
    TButton *OKButton;
    TLabel *Label1;
    TLabel *Label2;

public:
    virtual __fastcall TAboutBox(TComponent *AOwner);
};

extern PACKAGE TAboutBox *AboutBox;

#endif
