object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = '%s'
  ClientHeight = 300
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  DesignSize = (
    527
    300)
  PixelsPerInch = 96
  TextHeight = 15
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 27
    Height = 15
    Caption = '&Path:'
    FocusControl = PathEdit
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 72
    Height = 15
    Caption = '&Components:'
    FocusControl = PathList
  end
  object PathEdit: TEdit
    Left = 8
    Top = 24
    Width = 511
    Height = 23
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnChange = PathEditChange
  end
  object PathList: TListView
    Left = 8
    Top = 72
    Width = 511
    Height = 189
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        AutoSize = True
      end>
    ShowColumnHeaders = False
    TabOrder = 1
    ViewStyle = vsReport
    OnChange = PathListChange
  end
  object InsertButton: TButton
    Left = 8
    Top = 267
    Width = 75
    Height = 25
    Action = ListInsert
    Anchors = [akLeft, akBottom]
    TabOrder = 2
  end
  object DeleteButton: TButton
    Left = 89
    Top = 267
    Width = 75
    Height = 25
    Action = ListDelete
    Anchors = [akLeft, akBottom]
    TabOrder = 3
  end
  object MoveUpButton: TButton
    Left = 170
    Top = 267
    Width = 75
    Height = 25
    Action = ListMoveUp
    Anchors = [akLeft, akBottom]
    TabOrder = 4
  end
  object MoveDownButton: TButton
    Left = 251
    Top = 267
    Width = 75
    Height = 25
    Action = ListMoveDown
    Anchors = [akLeft, akBottom]
    TabOrder = 5
  end
  object ActionList1: TActionList
    Left = 32
    Top = 88
    object FileExit1: TFileExit
      Category = 'File'
      Caption = 'E&xit'
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
    object EditUndo1: TEditUndo
      Category = 'Edit'
      Caption = '&Undo'
      Hint = 'Undo|Reverts the last action'
      ImageIndex = 3
      ShortCut = 16474
    end
    object EditCut1: TEditCut
      Category = 'Edit'
      Caption = 'Cu&t'
      Hint = 'Cut|Cuts the selection and puts it on the Clipboard'
      ImageIndex = 0
      ShortCut = 16472
    end
    object EditCopy1: TEditCopy
      Category = 'Edit'
      Caption = '&Copy'
      Hint = 'Copy|Copies the selection and puts it on the Clipboard'
      ImageIndex = 1
      ShortCut = 16451
    end
    object EditPaste1: TEditPaste
      Category = 'Edit'
      Caption = '&Paste'
      Hint = 'Paste|Inserts Clipboard contents'
      ImageIndex = 2
      ShortCut = 16470
    end
    object EditSelectAll1: TEditSelectAll
      Category = 'Edit'
      Caption = 'Select &All'
      Hint = 'Select All|Selects the entire document'
      ShortCut = 16449
    end
    object EditDelete1: TEditDelete
      Category = 'Edit'
      Caption = '&Delete'
      Hint = 'Delete|Erases the selection'
      ImageIndex = 5
      ShortCut = 46
    end
    object ListInsert: TAction
      Category = 'List'
      Caption = '&Insert'
      Hint = 'Insert|Inserts a new item at the selection'
      ShortCut = 45
    end
    object ListDelete: TListControlDeleteSelection
      Category = 'List'
      Caption = '&Delete'
      Hint = 'Delete|Deletes the selection'
      ListControl = PathList
    end
    object ListMoveUp: TAction
      Category = 'List'
      Caption = 'Move &up'
      Hint = 'Move up|Moves the selection up'
    end
    object ListMoveDown: TAction
      Category = 'List'
      Caption = 'Move d&own'
      Hint = 'Move down|Moves the selection down'
    end
    object HelpContents1: THelpContents
      Category = 'Help'
      Caption = '%s &help'
      ImageIndex = 40
    end
    object AboutAction: TAction
      Category = 'Help'
      Caption = '&About %s'
      Hint = 'About|Describes the application'
      OnExecute = AboutActionExecute
    end
  end
  object MainMenu1: TMainMenu
    Left = 96
    Top = 88
    object File1: TMenuItem
      Caption = '&File'
      object Exit1: TMenuItem
        Action = FileExit1
      end
    end
    object Edit1: TMenuItem
      Caption = '&Edit'
      object Undo1: TMenuItem
        Action = EditUndo1
      end
      object EditN1: TMenuItem
        Caption = '-'
      end
      object Cut1: TMenuItem
        Action = EditCut1
      end
      object Copy1: TMenuItem
        Action = EditCopy1
      end
      object Paste1: TMenuItem
        Action = EditPaste1
      end
      object EditN2: TMenuItem
        Caption = '-'
      end
      object SelectAll1: TMenuItem
        Action = EditSelectAll1
      end
      object EditN3: TMenuItem
        Caption = '-'
      end
      object Delete1: TMenuItem
        Action = EditDelete1
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object Contents1: TMenuItem
        Action = HelpContents1
      end
      object HelpN1: TMenuItem
        Caption = '-'
      end
      object About1: TMenuItem
        Action = AboutAction
      end
    end
  end
end
