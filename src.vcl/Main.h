/*
 * pathedit
 * Copyright (C) 2012-2013  Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MainH
#define MainH 1

#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.StdActns.hpp>
#include <Vcl.ExtActns.hpp>
#include <Vcl.Menus.hpp>

// ---------------------------------------------------------------------------
class TMainForm : public TForm {
__published:
    // Contents
    TLabel *Label1;
    TLabel *Label2;
    TEdit *PathEdit;
    TListView *PathList;
    TButton *InsertButton;
    TButton *DeleteButton;
    TButton *MoveUpButton;
    TButton *MoveDownButton;

    // Actions
    TActionList *ActionList1;
    TFileExit *FileExit1;
    TEditUndo *EditUndo1;
    TEditCut *EditCut1;
    TEditCopy *EditCopy1;
    TEditPaste *EditPaste1;
    TEditSelectAll *EditSelectAll1;
    TEditDelete *EditDelete1;
    TAction *ListInsert;
    TListControlDeleteSelection *ListDelete;
    TAction *ListMoveUp;
    TAction *ListMoveDown;
    THelpContents *HelpContents1;
    TAction *AboutAction;

    // Menu items
    TMainMenu *MainMenu1;
    TMenuItem *File1;
    TMenuItem *Edit1;
    TMenuItem *Help1;
    TMenuItem *Exit1;
    TMenuItem *Undo1;
    TMenuItem *Cut1;
    TMenuItem *Copy1;
    TMenuItem *Paste1;
    TMenuItem *SelectAll1;
    TMenuItem *Delete1;
    TMenuItem *Contents1;
    TMenuItem *About1;
    TMenuItem *EditN1;
    TMenuItem *EditN2;
    TMenuItem *EditN3;
    TMenuItem *HelpN1;

    void __fastcall PathEditChange(TObject *Sender);
    void __fastcall PathListChange(TObject *Sender, TListItem *Item,
            TItemChange Change);
    void __fastcall AboutActionExecute(TObject *Sender);

public: // User declarations

    wchar_t Separator;

    /**
     * <stereotype>constructor</stereotype>
     */
    __fastcall TMainForm(TComponent *Owner);
    void __fastcall ApplicationMessage(MSG &Msg, bool &Handled);

private:
    void SaveSession();
    void RestoreSession();
};

// ---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
// ---------------------------------------------------------------------------
#endif
