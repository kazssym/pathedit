/*
 * pathedit
 * Copyright (C) 2012-2013  Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#pragma package(smart_init)

#include "About.h"
#include "Strings.hpp"
#include <StrUtils.hpp>

/*
 * Expands a macro as a wide string literal.
 */
#define W_(s) W__(s)
#define W__(s) L ## s

// ---------------------------------------------------------------------------
#pragma resource "*.dfm"
TMainForm *MainForm;

// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent *Owner)
    : TForm(Owner), Separator(L';') {

    TVarRec args[] = {Strings_ProductName};
    Caption = Format(Caption, args, 0);
    Contents1->Caption = Format(Contents1->Caption, args, 0);
    About1->Caption = Format(About1->Caption, args, 0);

    PathEdit->Text = GetEnvironmentVariable(L"PATH");

    RestoreSession();
    Application->OnMessage = ApplicationMessage;

    typedef HRESULT (WINAPI *REGISTERPROC)(PCWSTR, DWORD);
    REGISTERPROC proc1 = (REGISTERPROC) GetProcAddress
        (GetModuleHandle(kernel32), "RegisterApplicationRestart");
    if (proc1 != 0) {
        if (FAILED(proc1(0, 0))) {
            Application->MessageBox(L"Registration for restart failed.",
                L"Error", MB_OK);
        }
    }
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::PathEditChange(TObject *Sender) {
    TStringDynArray path(SplitString(PathEdit->Text, Separator));

    TNotifyEvent savedEvent = PathEdit->OnChange;
    PathEdit->OnChange = 0; // To prevent infinite recursion
    PathList->Items->BeginUpdate();
    try {
        PathList->Items->Clear();
        if (path.Length != 0) {
            for (int i = path.Low; i <= path.High; ++i) {
                TListItem *item = PathList->Items->Add();
                item->Caption = path[i];
            }
        }
    } __finally {
        PathList->Items->EndUpdate();
        PathEdit->OnChange = savedEvent;
    }
}

void __fastcall TMainForm::PathListChange(TObject *Sender, TListItem *Item,
    TItemChange Change)

{
    if (Change == ctText) {
        UnicodeString path;
        for (int i = 0; i != PathList->Items->Count; ++i) {
            if (i != 0)
                path += Separator;
            path += PathList->Items->Item[i]->Caption;
        }

        TLVChangeEvent savedEvent = PathList->OnChange;
        PathList->OnChange = 0; // To prevent infinite recursion
        try {
            PathEdit->Text = path;
            PathEdit->SelectAll();
        } __finally {
            PathList->OnChange = savedEvent;
        }
    }
}

void __fastcall TMainForm::AboutActionExecute(TObject *Sender) {
    if (AboutBox == 0)
        AboutBox = new TAboutBox(0);
    AboutBox->ShowModal();
}

void __fastcall TMainForm::ApplicationMessage(MSG &Msg, bool &Handled) {
    switch (Msg.message) {
    case WM_ENDSESSION:
        if (Msg.wParam) {
            SaveSession();
            Handled = true;
        }
        break;
    }
}

void TMainForm::SaveSession() {
    /* TODO: Implement SaveSession. */
}

void TMainForm::RestoreSession() {
    /* TODO: Implement RestoreSession. */
}
